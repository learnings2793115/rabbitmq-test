import json
import pika

RMQ_ADDRESS = "171.33.113.112"


class User:
    def __init__(self, name, age):
        self.name = name
        self.age = age


def manager(body: str):
    message = json.loads(body)
    objet = User(name=message["user"]["name"], age=message["user"]["age"])


def rmq_consumer():
    credentials = pika.PlainCredentials("pesko", "password")
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            host=RMQ_ADDRESS, credentials=credentials, virtual_host="twitch"
        )
    )
    channel = connection.channel()
    channel.queue_declare(queue="hello")

    def callback(ch, method, properties, body):
        print(f"Message received from {RMQ_ADDRESS}")
        manager(body)

    channel.basic_consume(
        queue="hello",
        auto_ack=True,
        on_message_callback=callback,
    )

    print(" [*] Waiting for messages. To exit press CTRL+C")
    channel.start_consuming()


def main():
    rmq_consumer()


if __name__ == "__main__":
    main()
