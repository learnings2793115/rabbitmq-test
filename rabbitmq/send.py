from typing import Any

import json
import pika


connection = None
channel = None


class Car:
    def __init__(self, color):
        self.color = color


def rmq_connection() -> None:
    global connection
    global channel

    # Create connection to RMQ.
    # HERE we are on other machine and we need to:
    # - Create rabbitmq user rabbitmqctl add_user "pesko"
    # - Add vhost rabbitmqctl add_vhost twitch
    # - Set user on vhost privileges rabbitmqctl set_permissions -p "twitch" "pesko" ".*" ".*" ".*"

    credentials = pika.PlainCredentials("pesko", "password")
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            host="localhost", credentials=credentials, virtual_host="twitch"
        )
    )
    channel = connection.channel()

    # Connection to queue.
    channel.queue_declare(queue="hello")


def rmq_send(msg: dict) -> None:
    # Exchange
    # Send a message to Consummer.
    channel.basic_publish(
        exchange="",
        routing_key="hello",
        body=json.dumps(msg),
        properties=pika.BasicProperties(content_type="application/json"),
    )

    print(f" [x] Sent '{msg}'")


def main():
    rmq_connection()
    message = {
        "user": {
            "name": "niord",
            "age": 1202,
        }
    }
    rmq_send(message)


if __name__ == "__main__":
    main()
    connection.close()
